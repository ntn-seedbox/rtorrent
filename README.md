# Docker rTorrent

Rtorrent in docker with RPC2 interface protected by auth basic.

Default username & password :
- admin:nimda

You should change password by modify auth file in `/etc/nginx/auth` :
```bash
htpasswd -c -B config/nginx/auth {MY_USERNAME}
```

## Features

- UI on port 80

- Watch directory
 
 Put torrent file in `/var/rtorrent/torrents` and rTorrent download it automatically
 

- XMLRPC protocol

You can interact with XML RPC `host/RPC2`


## Install

```bash
git clone https://gitlab.com/ntn-seedbox/rtorrent.git .

docker run 
  --env JWT_SECRET=<change> 
  --env COOKIE_SECRET=<change> 
  --env SESSION_SECRET=<change> 
  -p 80:80
  -v .data/dtorrent/downloaded:/var/rtorrent/downloaded
  -v .data/dtorrent/torrent:/var/rtorrent/torrents
  -v .data/dtorrent/session:/var/rtorrent/session
  -v .data/dtorrent/logs:/var/rtorrent/logs
  registry.gitlab.com/ntn-seedbox/rtorrent:latest
```

### docker-compose

```yaml
version: '3.8'
services:
  rtorrent:
    restart: always
    image: registry.gitlab.com/ntn-seedbox/rtorrent:latest
    ports:
      - "80:80"
    environment:
      JWT_SECRET: <change>
      COOKIE_SECRET: <change>
      SESSION_SECRET: <change>
    volumes:
      - .data/dtorrent/downloaded:/var/rtorrent/downloaded
      - .data/dtorrent/torrent:/var/rtorrent/torrents
      - .data/dtorrent/session:/var/rtorrent/session
      - .data/dtorrent/logs:/var/rtorrent/logs
```

### File system

rTorrent is launching with user `torrent` (1000:1000), so your volumes mounted should be accessible by 1000.

```bash
sudo chown -R 1000:1000 .data
```

### Volumes

- `/var/rtorrent/torrents` : directory of torrent files
- `/var/rtorrent/downloaded` : directory of torrent downloaded
- `/var/rtorrent/session` : (optional, useful for know historical for up/down rate)
- `/var/rtorrent/logs` : logs
