FROM debian:bullseye

ARG UUID=1000
ARG GGID=1000
ARG ENVIRONMENT=prod
ARG VERSION

ENV USER=torrent
ENV UUID=${UUID}
ENV GGID=${GGID}
ENV ENVIRONMENT=${ENVIRONMENT}
ENV VERSION=${VERSION}

RUN apt-get update && \
    apt-get install -y rtorrent \
    supervisor \
    nginx \
    curl

RUN curl -sL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get install -y nodejs

RUN mkdir -p /home/$USER/rtorrent /var/app /var/rtorrent/session /var/rtorrent/torrents /var/rtorrent/downloaded /var/rtorrent/logs /var/log/supervisor

RUN addgroup --system --gid $GGID $USER && \
    useradd -l --system --home-dir /home/$USER --uid $UUID --gid $GGID $USER && \
    chown -R $UUID:$GGID /var/rtorrent /home/$USER /var/app

#rTorrent
COPY --chown=$USER:$USER config/rtorrent /home/$USER/rtorrent
RUN chmod -R +x /home/$USER/rtorrent/events

# Supervisor
COPY --chown=root:root config/supervisor/$ENVIRONMENT /etc/supervisor

# Nginx
COPY --chown=root:root config/nginx/conf.d /etc/nginx/conf.d
COPY --chown=root:root config/nginx/auth /etc/nginx/auth
COPY --chown=root:root config/nginx/nginx.conf /etc/nginx/nginx.conf

#Entrypoint
COPY --chown=root:root config/entrypoint.sh /usr/local/bin/torrent
RUN chmod u+x /usr/local/bin/torrent

#App
RUN chown -R $USER:$USER /var/app
COPY --chown=$USER:$USER . /var/app

VOLUME /var/rtorrent/downloaded
VOLUME /var/rtorrent/torrents
VOLUME /var/rtorrent/logs

EXPOSE 80

WORKDIR /var/app

USER root

ENTRYPOINT ["torrent"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
