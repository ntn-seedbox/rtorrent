#!/bin/sh

/usr/bin/npm run webhook --prefix /var/app "$1" >> /var/rtorrent/logs/webhooks.log

exit 0
