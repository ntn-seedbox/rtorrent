#!/usr/bin/env bash
set -e

if [ "$1" = 'get' ]; then
    node /var/app/commands/torrent.js $2
    exit
fi

chown -R $USER:$USER /var/rtorrent
rm /var/rtorrent/session/rtorrent.lock || true
exec "$@"
