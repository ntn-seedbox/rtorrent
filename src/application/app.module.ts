import { AuthenticationModule } from '@application/authentication/authentication.module';
import { TorrentModule } from '@application/torrents/torrent.module';
import { Module } from '@nestjs/common';
import {HomeModule} from "@application/home/home.module";

@Module({
  imports: [
    AuthenticationModule,
    TorrentModule,
    HomeModule,
  ],
})
export class AppModule {}
