import { ApiProperty } from '@nestjs/swagger';

export class AuthenticationOutput {
  constructor(token: string) {
    this.token = token;
  }

  @ApiProperty()
  public token: string;
}
