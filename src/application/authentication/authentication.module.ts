import { AuthenticationController } from '@application/authentication/authentication.controller';
import { RtorrentStrategy } from '@application/authentication/strategies/rtorrent.strategy';
import { GenerateTokenInterface } from '@domain/authentication/ports/generate-token.interface';
import { AuthenticateRtorrent } from '@domain/authentication/use-cases/authenticate-rtorrent';
import { GenerateToken } from '@domain/authentication/use-cases/generate-token';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import {SessionStrategy} from "@application/authentication/strategies/session.strategy";

@Module({
  controllers: [AuthenticationController],
  imports: [PassportModule],
  providers: [
    {
      provide: GenerateTokenInterface,
      useClass: GenerateToken,
    },
    {
      provide: RtorrentStrategy,
      useFactory: () => {
        return new RtorrentStrategy(new AuthenticateRtorrent());
      }
    },
    {
      provide: SessionStrategy,
      useFactory: () => {
        return new SessionStrategy(new AuthenticateRtorrent());
      }
    },
  ]
})
export class AuthenticationModule {}
