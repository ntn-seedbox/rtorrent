import {PassportStrategy} from "@nestjs/passport";
import {Strategy} from "passport-custom";
import express from "express";
import {User} from "@domain/users/dto";
import {verify} from "jsonwebtoken";
import {Injectable, UnauthorizedException} from "@nestjs/common";
import {Server} from "@domain/torrents/dto";
import {AuthenticationRtorrentInterface} from "@domain/authentication/ports/authentication-rtorrent.interface";

@Injectable()
export class SessionStrategy extends PassportStrategy(Strategy, 'session') {
  constructor(
    protected authenticateRtorrent: AuthenticationRtorrentInterface,
  ) {
    super();
  }

  public async validate(req: express.Request): Promise<User> {
    try {
      const { password }: any = req.session;
      const decoded: any = verify(req.cookies.token, process.env.JWT_SECRET);

      return (await this.authenticateRtorrent.authenticate(
        new Server(decoded.server.host, decoded.server.port, decoded.server.endpoint, decoded.user, password)
      ));
    } catch (error: any) {
      throw new UnauthorizedException(error.message);
    }
  }
}
