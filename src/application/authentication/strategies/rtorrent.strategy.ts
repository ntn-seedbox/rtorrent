import { User } from '@domain/users/dto';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-custom';
import express from "express";
import {Server} from "@domain/torrents/dto";
import {AuthenticationRtorrentInterface} from "@domain/authentication/ports/authentication-rtorrent.interface";

@Injectable()
export class RtorrentStrategy extends PassportStrategy(Strategy, 'rtorrent') {
  constructor(
    protected authenticateRtorrent: AuthenticationRtorrentInterface,
  ) {
    super();
  }

  async validate(req: express.Request): Promise<User> {
    try {
      const {host, port, endpoint, user, password} = req.body
      const server = new Server(host, parseInt(port, 10), endpoint, user, password);
      return (await this.authenticateRtorrent.authenticate(server));
    } catch (error: any) {
      throw new UnauthorizedException();
    }
  }
}
