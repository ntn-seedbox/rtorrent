import { Injectable, SetMetadata } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class SessionGuard extends AuthGuard('session') {}

@Injectable()
export class RTorrentGuard extends AuthGuard('rtorrent') {}

export const Public = () => SetMetadata('IS_PUBLIC_KEY', true);
