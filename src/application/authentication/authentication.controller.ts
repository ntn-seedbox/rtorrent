import { RTorrentGuard } from '@application/authentication/guard';
import { GenerateTokenInterface } from '@domain/authentication/ports/generate-token.interface';
import {Controller, Get, Inject, Post, Render, Request, UseGuards, Res, Session, Req} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('authentication')
@ApiTags('Authentication')
export class AuthenticationController {
  constructor(
    @Inject(GenerateTokenInterface) protected generateToken: GenerateTokenInterface,
  ) {
  }

  @Get('/logout')
  async logout(
    @Res({ passthrough: true,}) res: any,
    @Session() session: Record<string, any>,
  ) {
    session.password = null;
    res.clearCookie('token')
    res.redirect(302, '/authentication');
  }

  @Get()
  @Render('login')
  async loginGet(
    @Req() req: any,
  ) {
    return {
      user: req.cookies.user,
      host: req.cookies.host || 'localhost',
      port: req.cookies.port || 80,
      endpoint: req.cookies.endpoint || '/RPC2',
    }
  }

  @ApiResponse({ status: 201, description: 'Authenticate user'})
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @Post()
  @UseGuards(RTorrentGuard)
  async login(
    @Request() req: any,
    @Res({ passthrough: true,}) res: any,
    @Session() session: Record<string, any>,
  ): Promise<void> {
    const { host, port, endpoint, user, password } = req.body;
    const token = await this.generateToken.generate(req.user);
    res.cookie('token', token);
    res.cookie('host', host);
    res.cookie('port', port);
    res.cookie('endpoint', endpoint);
    res.cookie('user', user);
    session.password = password;
    res.redirect(302, '/torrents');
  }
}
