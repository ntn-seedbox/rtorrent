import {Module} from "@nestjs/common";
import {HomeController} from "@application/home/home.controller";

@Module({
  controllers: [HomeController]
})
export class HomeModule {

}
