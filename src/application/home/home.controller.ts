import {Controller, Get, Res, UseGuards} from "@nestjs/common";
import {SessionGuard} from "@application/authentication/guard";

@Controller('')
export class HomeController {

  @Get()
  @UseGuards(SessionGuard)
  async home(
    @Res({ passthrough: true,}) res: any,
  ): Promise<void> {
    res.redirect(302, '/torrents');
  }
}
