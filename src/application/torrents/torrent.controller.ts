import { GetAllTorrentInterface } from '@domain/torrents/ports/get-all-torrent.interface';
import {
  Controller,
  Get,
  Inject,
  Param, ParseFilePipeBuilder,
  Post,
  Render,
  Request,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import {FileUpload, Torrent} from "@domain/torrents/dto";
import {SessionGuard} from "@application/authentication/guard";
import {StartingTorrentInterface} from "@domain/torrents/ports/starting-torrent.interface";
import {FileInterceptor} from "@nestjs/platform-express";
import {UploadTorrentInterface} from "@domain/torrents/ports/upload-torrent.interface";
import {StatsTorrentInterface} from "@domain/torrents/ports/stats-torrent.interface";

@Controller('torrents')
export class TorrentController {
  constructor(
    @Inject(GetAllTorrentInterface) protected getAllTorrent: GetAllTorrentInterface,
    @Inject(StartingTorrentInterface) protected startingTorrent: StartingTorrentInterface,
    @Inject(UploadTorrentInterface) protected uploadTorrent: UploadTorrentInterface,
    @Inject(StatsTorrentInterface) protected statTorrent: StatsTorrentInterface,
  ) {
  }

  @Get()
  @Render('index')
  @UseGuards(SessionGuard)
  async index(
    @Request() req: any,
  ) {
    const torrents = await this.getAllTorrent.getAllTorrents(req.user.server);
    const stats = await this.statTorrent.get(torrents);
    return {
      stats: {
        ...stats,
        downloaded: stats.getDownloadedAsString(),
        uploaded: stats.getUploadedAsString(),
        downRateGlobal: stats.getDownRateAsString(),
        upRateGlobal: stats.getUpRateAsString(),
      },
      connected: true,
      torrents: torrents.map((torrent: Torrent) => ({
        ...torrent,
        createdAt: torrent.getReadableDate(torrent.createdAt),
        startedAt: torrent.getReadableDate(torrent.startedAt, true),
        finishedAt: torrent.getReadableDate(torrent.finishedAt, true),
        loadedAt: torrent.getReadableDate(torrent.loadedAt, true),
        filesCount: torrent.files ? torrent.files.length : 0,
      }))
    };
  }

  @Post('/download')
  @UseGuards(SessionGuard)
  @UseInterceptors(FileInterceptor('torrent'))
  async download(
    @Request() req: any,
    @Res({ passthrough: true,}) res: any,
    @UploadedFile(new ParseFilePipeBuilder()
      .addFileTypeValidator({
        fileType: 'torrent',
      })
      .build({
        fileIsRequired: true,
      }),) file: Express.Multer.File
  ): Promise<void> {
    await this.uploadTorrent.upload(req.user.server, new FileUpload(file.buffer));
    res.redirect(302, '/torrents');
  }

  @Get('/remove/:hash')
  @UseGuards(SessionGuard)
  async remove(
    @Request() req: any,
    @Param('hash') hash: string,
    @Res({ passthrough: true,}) res: any,
  ) {
    await this.startingTorrent.remove(req.user.server, hash);
    res.redirect(302, '/torrents');
  }

  @Get('/pause/:hash')
  @UseGuards(SessionGuard)
  async pause(
    @Request() req: any,
    @Param('hash') hash: string,
    @Res({ passthrough: true,}) res: any,
  ) {
    await this.startingTorrent.pause(req.user.server, hash);
    res.redirect(302, '/torrents');
  }

  @Get('/play/:hash')
  @UseGuards(SessionGuard)
  async play(
    @Request() req: any,
    @Param('hash') hash: string,
    @Res({ passthrough: true,}) res: any,
  ) {
    await this.startingTorrent.play(req.user.server, hash);
    res.redirect(302, '/torrents');
  }

  @Get('/start/:hash')
  @UseGuards(SessionGuard)
  async start(
    @Request() req: any,
    @Param('hash') hash: string,
    @Res({ passthrough: true,}) res: any,
  ) {
    await this.startingTorrent.start(req.user.server, hash);
    res.redirect(302, '/torrents');
  }

  @Get('/stop/:hash')
  @UseGuards(SessionGuard)
  async stop(
    @Request() req: any,
    @Param('hash') hash: string,
    @Res({ passthrough: true,}) res: any,
  ) {
    await this.startingTorrent.stop(req.user.server, hash);
    res.redirect(302, '/torrents');
  }
}
