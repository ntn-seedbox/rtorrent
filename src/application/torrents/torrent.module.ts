import { TorrentController } from '@application/torrents/torrent.controller';
import { GetAllTorrentInterface } from '@domain/torrents/ports/get-all-torrent.interface';
import { GetAllTorrent } from '@domain/torrents/use-cases/get-all-torrent';
import { Module } from '@nestjs/common';
import {StartingTorrentInterface} from "@domain/torrents/ports/starting-torrent.interface";
import {StartingTorrent} from "@domain/torrents/use-cases/starting-torrent";
import {UploadTorrentInterface} from "@domain/torrents/ports/upload-torrent.interface";
import {UploadTorrent} from "@domain/torrents/use-cases/upload-torrent";
import {StatsTorrentInterface} from "@domain/torrents/ports/stats-torrent.interface";
import {StatsTorrent} from "@domain/torrents/use-cases/stats-torrent";

@Module({
  controllers: [TorrentController],
  providers: [
    {
      provide: GetAllTorrentInterface,
      useClass: GetAllTorrent,
    },
    {
      provide: StartingTorrentInterface,
      useClass: StartingTorrent,
    },
    {
      provide: UploadTorrentInterface,
      useClass: UploadTorrent,
    },
    {
      provide: StatsTorrentInterface,
      useClass: StatsTorrent,
    },
  ]
})
export class TorrentModule {}
