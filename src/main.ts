import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from '@application/app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import { join, resolve } from 'path';
import hbs from 'hbs';
import {HttpExceptionFilter} from "@application/http-exception-filter";
import * as process from "node:process";
import {MemoryStore} from "express-session";

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalPipes(
    new ValidationPipe(),
  );

  app.useGlobalFilters(new HttpExceptionFilter());
  app.use(cookieParser(process.env.COOKIE_SECRET));

  app.use(
    session({
      saveUninitialized: false,
      cookie: { maxAge: 86400000 },
      store: new MemoryStore(),
      resave: false,
      secret: process.env.SESSION_SECRET,
    }),
  );

  hbs.registerPartials(join(__dirname, 'application', '_partials'));
  hbs.registerHelper('global', (name: string) => {
    return {
      version: `v${process.env.VERSION}`
    }[name];
  });

  app.useStaticAssets(join(__dirname, 'application', '_assets'));
  app.useStaticAssets(join(resolve(), 'node_modules/bootstrap/dist/js'));

  app.setBaseViewsDir(join(__dirname, 'application', '_views'));
  app.setViewEngine('hbs');

  await app.listen(8080);
}
bootstrap();
