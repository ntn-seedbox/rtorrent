import {Converter} from "@domain/torrents/dto/converter";

export class Torrent {
  public progress: number;
  public downloadedStr: string;
  public uploadedStr: string;
  public sizeStr: string;
  public downRateStr: string;
  public upRateStr: string;
  public createdAt: Date;
  public startedAt: Date;
  public finishedAt: Date;
  public loadedAt: Date;
  public totalSeeders: number = 0;
  public totalLeechers: number = 0;

  constructor(
    public hash: string,
    public name: string,
    public file: string,
    public path: string,
    public isActive: boolean,
    public isOpen: boolean,
    public downloaded: number,
    public uploaded: number,
    public size: number,
    public ratio: number,
    public downRate: number,
    public upRate: number,
    public isComplete: boolean,
    createdAt: string,
    startedAt: string,
    finishedAt: string,
    loadedAt: string,
    public trackers?: any[],
    public files?: any[],
  ) {
    this.progress = Number((downloaded * 100 / size).toFixed(2));
    this.downloadedStr = Converter.byteSizeToString(downloaded)
    this.uploadedStr = Converter.byteSizeToString(uploaded);
    this.sizeStr = Converter.byteSizeToString(size);
    this.downRateStr = Converter.byteSizeToString(downRate);
    this.upRateStr = Converter.byteSizeToString(upRate);
    this.createdAt = new Date(createdAt)
    this.startedAt = new Date(startedAt)
    this.finishedAt = new Date(finishedAt)
    this.loadedAt = new Date(loadedAt)
    this.files = this.files.map((file) => ({
      ...file,
      sizeStr: Converter.byteSizeToString(file.length)
    }))

    for (const tracker of this.trackers) {
      this.totalSeeders += tracker.seeders;
      this.totalLeechers += tracker.leechers;
    }
  }

  public getReadableDate(date: Date, withTime?: boolean) {
    if (withTime) {
      return new Intl.DateTimeFormat('en-US', {
        month: '2-digit',
        day: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
      }).format(date);
    }

    return new Intl.DateTimeFormat('en-US', {
      month: '2-digit',
      day: '2-digit',
      year: 'numeric',
    }).format(date);
  }
}
