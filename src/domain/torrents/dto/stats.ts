import {Converter} from "@domain/torrents/dto/converter";

export class Stats {
  constructor(
    public totalTorrent: number,
    public totalSeeders: number,
    public totalLeechers: number,
    public downRateGlobal: number = 0,
    public upRateGlobal: number = 0,
    public downloaded: number = 0,
    public uploaded: number = 0,
    public ratio: number = 0,
  ) {
  }

  getDownloadedAsString() {
    return Converter.byteSizeToString(this.downloaded);
  }

  getUploadedAsString() {
    return Converter.byteSizeToString(this.uploaded);
  }

  getDownRateAsString() {
    return Converter.byteSizeToString(this.downRateGlobal);
  }

  getUpRateAsString() {
    return Converter.byteSizeToString(this.upRateGlobal);
  }
}
