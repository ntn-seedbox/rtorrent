export class Server {
  constructor(
    public host: string,
    public port: number,
    public endpoint: string,
    public user: string,
    public password: string,
  ) {
  }
}
