export class Converter {
  public static byteSizeToString = (bytes: number): string => {
    let i = 0;
    while (bytes > 1024) {
      i++;
      bytes /= 1024;

      if (i === 4) {
        break;
      }
    }

    return `${bytes > 0 ? bytes.toFixed(2) : bytes} ${i === 0 ? 'o' : i === 1 ? 'Ko' : i === 2 ? 'Mo' : i === 3 ? 'Go' : 'To'}`
  };
}
