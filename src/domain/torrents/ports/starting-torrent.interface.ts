import {Server} from "@domain/torrents/dto";

export interface StartingTorrentInterface {
  play(server: Server, hash: string): Promise<boolean>;
  pause(server: Server, hash: string): Promise<boolean>;
  start(server: Server, hash: string): Promise<boolean>;
  stop(server: Server, hash: string): Promise<boolean>;
  remove(server: Server, hash: string): Promise<boolean>;
}

export const StartingTorrentInterface = Symbol('StartingTorrentInterface');
