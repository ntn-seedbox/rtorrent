import {Server, Torrent} from '@domain/torrents/dto';

export interface GetAllTorrentInterface {
  getAllTorrents(server: Server): Promise<Torrent[]>;
}

export const GetAllTorrentInterface = Symbol('GetAllTorrentInterface');
