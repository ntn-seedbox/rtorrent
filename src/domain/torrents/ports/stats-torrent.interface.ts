import {Stats, Torrent} from "@domain/torrents/dto";

export interface StatsTorrentInterface {
  get(torrents: Torrent[]): Promise<Stats>
}

export const StatsTorrentInterface = Symbol('StatsTorrentInterface');
