import {FileUpload, Server} from "@domain/torrents/dto";

export interface UploadTorrentInterface {
  upload(server: Server, file: FileUpload): Promise<void>;
}

export const UploadTorrentInterface = Symbol('UploadTorrentInterface');
