import {StatsTorrentInterface} from "@domain/torrents/ports/stats-torrent.interface";
import { Torrent, Stats } from "../dto";
import {Injectable} from "@nestjs/common";

@Injectable()
export class StatsTorrent implements StatsTorrentInterface {
  async get(torrents: Torrent[]): Promise<Stats> {
    const stat = new Stats(torrents.length, 0, 0);

    for (const torrent of torrents) {
      stat.totalSeeders += torrent.totalSeeders;
      stat.totalLeechers += torrent.totalLeechers;
      stat.downRateGlobal += torrent.downRate;
      stat.upRateGlobal += torrent.upRate;
      stat.downloaded += torrent.downloaded;
      stat.uploaded += torrent.uploaded;
    }

    stat.ratio = Math.round((stat.uploaded / stat.downloaded) * 1e2 ) / 1e2;
    return stat;
  }
}
