import {StartingTorrentInterface} from "@domain/torrents/ports/starting-torrent.interface";
import {Injectable} from "@nestjs/common";
import { Server } from "../dto";
import wTorrent, {TorrentClientWrapper} from "wrapper-torrents";

@Injectable()
export class StartingTorrent implements StartingTorrentInterface {
  private async connect(server: Server): Promise<TorrentClientWrapper> {
    return wTorrent({
      name: 'LocalRtorrent',
      client: 'rtorrent',
      host: server.host,
      port: server.port,
      endpoint: server.endpoint,
      user: server.user,
      password: server.password,
    });
  }

  async play(server: Server, hash: string): Promise<boolean> {
      const client = await this.connect(server);
      return client.play(hash);
  }

  async pause(server: Server, hash: string): Promise<boolean> {
    const client = await this.connect(server);
    return client.pause(hash);
  }

  async start(server: Server, hash: string): Promise<boolean> {
    const client = await this.connect(server);
    return client.start(hash);
  }

  async stop(server: Server, hash: string): Promise<boolean> {
    const client = await this.connect(server);
    return client.stop(hash);
  }

  async remove(server: Server, hash: string): Promise<boolean> {
    const client = await this.connect(server);
    return client.remove(hash);
  }
}
