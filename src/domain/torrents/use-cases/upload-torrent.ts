import {UploadTorrentInterface} from "@domain/torrents/ports/upload-torrent.interface";
import { Server, FileUpload } from "../dto";
import {Injectable} from "@nestjs/common";
import wTorrent from "wrapper-torrents";

@Injectable()
export class UploadTorrent implements UploadTorrentInterface {

  async upload(server: Server, file: FileUpload): Promise<void> {
    const client = await wTorrent({
      name: 'LocalRtorrent',
      client: 'rtorrent',
      host: server.host,
      port: server.port,
      endpoint: server.endpoint,
      user: server.user,
      password: server.password,
    });

    await client.createFromBuffer(file.buffer);
  }
}
