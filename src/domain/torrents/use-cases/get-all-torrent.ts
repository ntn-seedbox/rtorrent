import { GetAllTorrentInterface } from '@domain/torrents/ports/get-all-torrent.interface';
import wTorrent, { Torrent as WTorrent } from 'wrapper-torrents';
import {Server, Torrent} from "@domain/torrents/dto";

export class GetAllTorrent implements GetAllTorrentInterface {
  async getAllTorrents(server: Server): Promise<Torrent[]> {
    const client = await wTorrent({
      name: 'LocalRtorrent',
      client: 'rtorrent',
      host: server.host,
      port: server.port,
      endpoint: server.endpoint,
      user: server.user,
      password: server.password,
    });

    const torrents: WTorrent[] = await client.get();
    return torrents
      .map((torrent: WTorrent) => new Torrent(
        torrent.hash,
        torrent.name,
        torrent.torrentFile,
        torrent.path,
        torrent.isActive,
        torrent.isOpen,
        torrent.downloaded,
        torrent.uploaded,
        torrent.length,
        torrent.ratio,
        torrent.downRate,
        torrent.upRate,
        torrent.isComplete,
        torrent.createdAt,
        torrent.startedAt,
        torrent.finishedAt,
        torrent.loadedAt,
        torrent.trackers,
        torrent.files,
      ))
      .sort((a: Torrent, b: Torrent) => b.loadedAt.getTime() - a.loadedAt.getTime());
  }
}
