import {Server} from "@domain/torrents/dto";

export class User {
  constructor(
    public user: string,
    public password?: string,
    public server?: Server
  ) {
  }
}
