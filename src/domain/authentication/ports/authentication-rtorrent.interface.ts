import { User } from '@domain/users/dto';
import {Server} from "@domain/torrents/dto";

export interface AuthenticationRtorrentInterface {
  authenticate(server: Server): Promise<User>;
}

export const AuthenticationRtorrentInterface = Symbol('AuthenticationRtorrentInterface');
