import { User } from '@domain/users/dto';

export interface GenerateTokenInterface {
  generate(user: User): Promise<string>;
}

export const GenerateTokenInterface = Symbol('GenerateTokenInterface');
