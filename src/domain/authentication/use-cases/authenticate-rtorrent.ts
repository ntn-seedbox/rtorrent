import { User } from '@domain/users/dto';
import {AuthenticationRtorrentInterface} from "@domain/authentication/ports/authentication-rtorrent.interface";
import {Server} from "@domain/torrents/dto";
import {UnauthorizedException} from "@nestjs/common";
import wTorrent from 'wrapper-torrents'

export class AuthenticateRtorrent implements AuthenticationRtorrentInterface {
  async authenticate(server: Server): Promise<User> {
    try {
      await wTorrent({
        name: 'LocalRtorrent',
        client: 'rtorrent',
        host: server.host,
        port: server.port,
        endpoint: server.endpoint,
        user: server.user,
        password: server.password,
      });

      return new User(server.user, server.password, server);
    } catch(err: any) {
      throw new UnauthorizedException();
    }
  }
}
