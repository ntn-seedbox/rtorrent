import { GenerateTokenInterface } from '@domain/authentication/ports/generate-token.interface';
import { User } from '@domain/users/dto';
import { sign } from 'jsonwebtoken';
import * as process from "node:process";

export class GenerateToken implements GenerateTokenInterface {
  async generate(user: User): Promise<string> {
    return sign(
      {
        user: user.user,
        server: {
          host: user.server.host,
          port: user.server.port,
          endpoint: user.server.endpoint,
        },
      },
      process.env.JWT_SECRET,
      {
        algorithm: 'HS256',
        expiresIn: '1w',
      }
    );
  }
}
